# Name of the React App is LOST-IN-TRANSLATIONS

# DATABASE: https://dd-dp-n-api.herokuapp.com/translations

# HEROKU LINK: https://lost-in-translations-dd-dp-nv.herokuapp.com/

# This is a Group work:  
 
 Done by Devendra P , Dominik  Drat and Neha

# DESCRIPTION:
- The App starts when an user signs in and clicks on the submit button.
- The name of the User is stored to the Translation API. 
- Once the name has been stored in the API, the app redirects to translation page.
- The browse local storage is used to manage the session. 
- In translation page the user types in the input box the text he wants to translate and once he clicks on the translation button then the translation of the text happens into American Sign Language (ASL)
- The translations are done using the API: https://github.com/dewald-els/noroff-assignment-api
- The Sign language characters will appear under the translation input box.
- The user who is logged in is shown in the Navbar.
- When the user clicks on his name. The latest 10 translations done by the user can be seen on this profile page.
- When user logges out of the profile page he is directed to the signIn page.


# VISUALS
The Component tree is used to show the structure and features of the component.

# Getting Started with Create React App

This project was bootstrapped with [Create React App]

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:8081/](http://localhost:8081/) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests] for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
