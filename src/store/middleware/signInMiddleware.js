import { SignInAPI } from "../../components/Signin/SignInAPI"
import { ACTION_SIGNIN_ATTEMPT, ACTION_SIGNIN_SUCCESS, signInErrorAction, signInSuccessAction } from "../actions/signInActions"
import { sessionSetAction } from "../actions/sessionActions"

export const signInMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SIGNIN_ATTEMPT) {
        SignInAPI.signIn(action.payload)
            .then(profile => {
                if (profile.length === 0) {
                    SignInAPI.register(action.payload)
                        .then(profile => {
                            dispatch(signInSuccessAction(profile))
                        })// the user stored is an object not array
                        .catch(error => {
                            dispatch(signInErrorAction(error.message))
                        })
                } else {
                    dispatch(signInSuccessAction([profile[0]]))
                }
            })
            .catch(error => {
                console.log(action.payload)
                dispatch(signInErrorAction(error.message))
            })
    }
   //
    if (action.type === ACTION_SIGNIN_SUCCESS) {
        dispatch(sessionSetAction( action.payload[0] ? action.payload[0] : action.payload ))
    }
}