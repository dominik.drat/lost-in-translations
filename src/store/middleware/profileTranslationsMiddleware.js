import { ProfileTranslationsAPI } from "../../components/Profile/ProfileTranslationsAPI"
import { API_ERROR_INVALID_AUTH } from "../../utils/apiUtils"
import { ACTION_PROFILE_TRANSLATIONS_FETCHING, profileTranslationsErrorAction, profileTranslationsSetAction } from "../actions/profileTranslationsActions"
import { sessionExpiredAction } from "../actions/sessionActions"

export const profileTranslationsMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_PROFILE_TRANSLATIONS_FETCHING) {
        ProfileTranslationsAPI.getProfileTranslations(action.payload)
            .then(translations => {
                console.log("Translation history:" + JSON.stringify(translations[0]))
                dispatch(profileTranslationsSetAction(translations[0]))
            })
            .catch(({ message }) => {
                if (message === API_ERROR_INVALID_AUTH) {
                    dispatch(sessionExpiredAction())
                } else {
                    dispatch(profileTranslationsErrorAction(message))
                }
            })
    }
}