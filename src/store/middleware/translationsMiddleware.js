import { ACTION_SAVE_TRANSLATIONS } from "../actions/translationsAction";

import { TranslationAPI } from "../../components/Translation/TranslationAPI";

export const translationsMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_SAVE_TRANSLATIONS) {
      TranslationAPI.register(action.payload)
        .then((translation) => {})
        .catch((error) => {});
    }
  };
