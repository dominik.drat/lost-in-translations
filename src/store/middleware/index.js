import { applyMiddleware } from "redux"
import { profileTranslationsMiddleware } from "./profileTranslationsMiddleware"
import { sessionMiddleware } from "./sessionMiddleware"
import { signInMiddleware } from "./signInMiddleware"
import { translationsMiddleware } from "./translationsMiddleware"


export default applyMiddleware(
    signInMiddleware,
    sessionMiddleware,
    profileTranslationsMiddleware,
    translationsMiddleware
)