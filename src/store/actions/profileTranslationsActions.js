export const ACTION_PROFILE_TRANSLATIONS_FETCHING = '[profile-translations] FETCHING'
export const ACTION_PROFILE_TRANSLATIONS_SET = '[profile-translations] SET'
export const ACTION_PROFILE_TRANSLATIONS_ERROR = '[profile-translations] ERROR'

export const profileTranslationsFetchingAction = userId => ({
    type: ACTION_PROFILE_TRANSLATIONS_FETCHING,
    payload: userId
})

export const profileTranslationsSetAction = translations => ({
    type: ACTION_PROFILE_TRANSLATIONS_SET,
    payload: translations
})

export const profileTranslationsErrorAction = error => ({
    type: ACTION_PROFILE_TRANSLATIONS_ERROR,
    payload: error
})