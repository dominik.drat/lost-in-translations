export const ACTION_SIGNIN_ATTEMPT = '[signin] ATTEMPT'
export const ACTION_SIGNIN_SUCCESS = '[signin] SUCCESS'
export const ACTION_SIGNIN_ERROR = '[signin] ERROR'

export const signInAttemptAction = credentials => ({
    type: ACTION_SIGNIN_ATTEMPT,
    payload: credentials
})

export const signInSuccessAction = profile => ({
    type: ACTION_SIGNIN_SUCCESS,
    payload: profile
})

export const signInErrorAction = error => ({
    type: ACTION_SIGNIN_ERROR,
    payload: error
})