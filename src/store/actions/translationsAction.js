export const ACTION_SAVE_TRANSLATIONS = '[translation] SAVE'


export const saveTranslations = translations => ({
    type: ACTION_SAVE_TRANSLATIONS,
    payload: translations
})