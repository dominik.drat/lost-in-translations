import {
  ACTION_SESSION_CLEAR,
  ACTION_SESSION_LOGOUT,
  ACTION_SESSION_SET, ACTION_TRANSLATIONS_CLEAR,
} from "../actions/sessionActions"

const initialState = {
  id: '',
  username: '',
  translations: [],
  token: '',
  signedIn: false,
}
export const sessionReducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        ...state,
        ...action.payload,
        signedIn: true,
      }
    case ACTION_SESSION_CLEAR:
      return {
        ...initialState,
        signedIn: false
      }
    case ACTION_SESSION_LOGOUT:
      return {
        ...state,
      }
    case ACTION_TRANSLATIONS_CLEAR:
      return {
        ...state,
        translations: []
      }
    default:
      return state
  }
}