import { combineReducers } from "redux"
import { signInReducer } from "./signInReducer"
import { sessionReducer } from "./sessionReducer"
import { ACTION_SESSION_CLEAR } from "../actions/sessionActions"


const appReducer = combineReducers({
  signIn: signInReducer,
  session: sessionReducer,
})
const rootReducer = (state, action) => {
  if (action.type === ACTION_SESSION_CLEAR) {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer