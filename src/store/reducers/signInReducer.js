import {
  ACTION_SIGNIN_ATTEMPT,
  ACTION_SIGNIN_ERROR,
  ACTION_SIGNIN_SUCCESS,
} from "../actions/signInActions"
const initialState = {
  signInAttempting: false,
  signInError: ''
}
export const signInReducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case ACTION_SIGNIN_ATTEMPT:
      return {
        ...state,
        signInAttempting: true,
        signInError: '',
      }
    case ACTION_SIGNIN_SUCCESS:
      return {
        ...initialState,
      }
    case ACTION_SIGNIN_ERROR:
      return {
        ...state,
        signInAttempting: false,
        signInError: action.payload,
      }
    default:
      return state
  }
}