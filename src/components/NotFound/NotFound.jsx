import { Link } from "react-router-dom";
const NotFound = () => {
  return (
    <div className="card card-body w-50 mx-auto my-5 mt-5" align="center">
      <main>
        <h4>Page Not Found.</h4>
        <Link to="/">Go to Start Page.</Link>
      </main>
    </div>
  );
};
export default NotFound;
