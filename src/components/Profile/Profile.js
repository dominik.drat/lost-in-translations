import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import {sessionClearTranslations, sessionLogoutAction} from '../../store/actions/sessionActions'
import ProfileTranslations from './ProfileTranslations'

export const Profile = () => {
    const history = useHistory()
    const { signedIn } = useSelector((state) => state.session)
    useEffect(() => {
        if (!signedIn) {
            history.push('/')
        }
    })

    const dispatch = useDispatch()
    const { username = '' } = useSelector(state => state.session)

    const onLogoutClick = () => {
        dispatch(sessionLogoutAction())
        history.push('/');
    }

    const onClearTranslationsClick= () => {
        dispatch(sessionClearTranslations())
    }


    return (

        <div className="mx-auto my-5 mt-5" align="center">
            <main>
                <h1>Hi, {username}</h1>
                <p>Welcome to your profile 😊</p>
                <button className="btn btn-danger" onClick={onLogoutClick}>Logout</button>
                <button className="btn btn-danger" onClick={onClearTranslationsClick}>Clear Translations</button>
                <ProfileTranslations />
            </main>
        </div>
    )
}
export default Profile