import { useSelector } from "react-redux"

const ProfileTranslations = () => {

    const { translations } = useSelector(state => state.session)

    return (
        <>
            <h4 className="jumbotron mt-5 mb-3">Translation History: Your Total Translations Count - {translations.length}. </h4>
            <h5 className="mb-3">Last ten translations:</h5>
            <section>
                {translations.slice(-10).reverse().map(function (translation, index) {
                    return <div className="btn btn-lg btn-block btn-outline-primary" key={index}>{translation}</div>
                }
                )}
            </section>
        </>

    )
}
export default ProfileTranslations