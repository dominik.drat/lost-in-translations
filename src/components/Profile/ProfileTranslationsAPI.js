import { API_ERROR_INVALID_AUTH } from "../../utils/apiUtils"

export const ProfileTranslationsAPI = {
    getProfileTranslations(userId) {
        const { token } = JSON.parse(localStorage.getItem('snte-ss'))

        return fetch(`https://dd-dp-n-api.herokuapp.com/translations/${userId}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }).then(async response => {
            if (response.status === 401) {
                throw new Error(API_ERROR_INVALID_AUTH)
            }
            else if (!response.ok) {
                const { error = 'An unknown error occurred' } = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
    }
}