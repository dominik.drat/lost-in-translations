export const SignInAPI = {
  signIn(credentials) {
    return fetch(`https://dd-dp-n-api.herokuapp.com/translations?username=${credentials.username}`, {
      method: 'GET'
    })
      .then(async response => {
        if (!response.ok) {
          const { error = 'An unknown error occurred' } = await response.json()
          throw new Error(error)
        }
        return response.json()
      })
  },
  register(credentials) {
    return fetch('https://dd-dp-n-api.herokuapp.com/translations', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'X-API-Key': 'dd-dp-2021'
      },
      body: JSON.stringify({
        username: credentials.username,
        translations: []
      })
    })
      .then(async response => {
        if (!response.ok) {
          const { error = 'An unknown error occurred' } = await response.json()
          throw new Error(error)
        }
        return response.json()
      })
  }
}
