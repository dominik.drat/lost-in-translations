import { useState} from "react"
import { useDispatch, useSelector } from "react-redux"
import { Redirect } from "react-router"
import icons from "../../assets/icons.png"
import "../Signin/SignIn.css"

import { signInAttemptAction } from "../../store/actions/signInActions"

const SignIn = () => {
  const dispatch = useDispatch()

  const [credentials, setCredentials] = useState({
    username: ''
  })

  const { signedIn } = useSelector(state => state.session)

  const onInputChange = e => {
    setCredentials({
      ...credentials,
      [e.target.id]: e.target.value
    })
  }

  const onSignInSubmit = (e) => {
    e.preventDefault()
    dispatch(signInAttemptAction(credentials))
  }

  return (
    <>
      {signedIn && <Redirect to="/translation" />}
      {!signedIn &&
        <form onSubmit={onSignInSubmit}>
          <div className="bg" style={{ backgroundImage: `url(${icons})` }}>
            <h1> Lost in Translation!! </h1>
            <div className="form-group">
              <label htmlFor="username" className="form-label"></label>
              <input
                id="username"
                type="text"
                placeholder="Enter UserName"
                onChange={onInputChange}
                className="input"
                aria-describedby="username"
                required
              />
              <input type="submit" className="ml-2 input-button btn-primary" />
            </div>
          </div>
        </form>
      }
    </>
  )
}
export default SignIn
