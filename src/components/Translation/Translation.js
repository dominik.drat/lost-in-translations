import { useSelector, useDispatch } from "react-redux"
import { useEffect, useState } from "react"
import { useHistory } from "react-router-dom"
import TranslationArea from "./TranslationArea/TranslationArea"
import TranslationForm from "./TranslationForm"

import { saveTranslations } from "../../store/actions/translationsAction"


const Translation = () => {
    const dispatch = useDispatch()

    const history = useHistory()
    const loggedInSession  = useSelector((state) => state.session);

    useEffect(() => {
        if (!loggedInSession.signedIn) {
            history.push('/')
        }
    })

    const [translationItem, setTranslationItem] = useState({
        translation: '',
        letters: []
    })

    const [translations, setTranslations] = useState([])

    const handleInputChange = event => {
        setTranslationItem({
            ...translationItem,
            translation: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        setTranslationItem({
            ...translationItem,
            letters: translationItem.translation.toLowerCase().split('')
        })
        if (translationItem.translation.length > 40) {
            alert("word is too long ")
            setTranslationItem({
                ...translationItem,
                translation: "",
            })
        } else if (translationItem.translation.match(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)) {
            setTranslations(translations => [...translations, translationItem.translation])
    // get all existing translation
            let { translations = [] } = loggedInSession;
            //add new translation into the variable and dispatch the value
            translations.push(translationItem.translation);
            loggedInSession.translations = translations;
            console.log("loggedInSession", loggedInSession);
            dispatch(saveTranslations(loggedInSession))
        }
        else {
            alert("Invalid input - enter a phrase consisting of only letters and max one space")
            setTranslationItem({
                ...translationItem,
                translation: "",
            })
        }
    }



    return (
        <div className="mx-auto my-5 mt-5" align="center">
            <main>
                <TranslationForm translation={translationItem.translation} handleSubmit={handleSubmit} handleInputChange={handleInputChange} />

                {translationItem.letters.length > 0 &&
                    <div className="mt-3 p-4 w-75 m-auto">
                        <h3 className="text-center">Translation</h3>
                        <TranslationArea letters={translationItem.letters} />
                    </div>
                }
            </main>
        </div>
    )
}

export default Translation
