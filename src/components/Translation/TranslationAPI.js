export const TranslationAPI = {

    register(credentials) {
      console.log(credentials);
      const { id, translations = [] } = credentials;
      return fetch(`https://dd-dp-n-api.herokuapp.com/translations/${id}`, {
        method: "PATCH",
        headers: {
          "Content-type": "application/json",
          "X-API-Key": "dd-dp-2021",
        },
        body: JSON.stringify({
          translations: translations,
        }),
      }).then(async (response) => {
        if (!response.ok) {
          const { error = "An unknown error occurred" } = await response.json();
          throw new Error(error);
        }
        return response.json();
      });
    },
  };