import { BrowserRouter, Switch, Route } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"

import SignIn from "./components/Signin/SignIn"
import Translation from "./components/Translation/Translation"
import NotFound from "./components/NotFound/NotFound"
import { Navbar } from "./components/Navbar/Navbar"
import Profile from "./components/Profile/Profile"

function App() {
  return (
    <BrowserRouter>
      <div className="container mt-3">
        <Navbar />
        <Switch>
          <Route path="/" exact component={SignIn} />
          <Route path="/signin" exact component={SignIn} />
          <Route path="/translation" component={Translation} />
          <Route path="/profile" component={Profile} />
          <Route path="/*" component={NotFound} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}
export default App
